//Import my module
var vector = require("./vector3.js");
console.log("This is my Module:", vector);


//Generate example vectors and array.
var u = { x: 0.1231, y: 1.5431, z: 3.464 };
var v = { x: 3.456, y: 4.871, z: 1.1544 };
var w = { x: 9.1114, y: 7.1254, z: 2.9781 };
var vectorArray = [u, v, w];

//iterate vector to calculate magnitude
vectorArray.forEach(v => {
    console.log("Magnitude of vector ", vector.Magnitude(v));
});

//iterate vector to calculate square magnitude
vectorArray.forEach(v => {
    console.log("Square magnitude of vector ", vector.SqrMagnitude(v));
});

//Display other examples of Vector3 module uses
console.log("Dot product of two vector: ", vector.Dot(u, v));
console.log("Cross product of two vector: ", vector.Cross(v, w));
console.log("Scale a vector by another: ", vector.Scale(u, w));
console.log("Static vector at the origin.", vector.Properties.Zero);
console.log("Static vector pointing at up direction.", vector.Properties.Up);
console.log("The angle between two vectors: ", vector.Angle(w, v), " degrees");
console.log("A random vector3: ", vector.Properties.Random);
console.log("Multiples functions in one call: ", vector.Scale(vector.Properties.Random, vector.Properties.Random));